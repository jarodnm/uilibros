package navarro.jarod.ui;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;

import navarro.jarod.logica.tl.Controller;

public class UI {
    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static PrintStream out = System.out;
    static Controller gestor = new Controller();

    public static void main(String[] args) throws IOException {
        Boolean noSalir = true;
        int opcion;
        do {
            menu();
            out.println("Digite su opción");
            opcion = Integer.parseInt(in.readLine());
            noSalir = ejecutarAccion(opcion);
        } while (noSalir);
    }

    public static void menu() {
        out.println();
        out.println("1. Registrar Autor");
        out.println("2. Registrar Novela");
        out.println("3. Registrar Cuento");
        out.println("4. Listar autores");
        out.println("5. Listar Libros");
        out.println("6. Salir");
    }

    public static boolean ejecutarAccion(int opcion) throws IOException {
        boolean noSalir = true;
        switch (opcion) {
            case 1:
                registrarAutor();
                break;
            case 2:
                registrarNovela();
                break;
            case 3:
                registrarCuento();
                break;
            case 4:
                listarAutores();
                break;
            case 5:
                listarLibros();
                break;
            case 6:
                out.println("Gracias por usar el programa");
                noSalir = false;
                break;
            default:
                out.println("Digite una opción valida");
                break;
        }
        return noSalir;
    }

    public static void registrarAutor() throws IOException {
        String nombre;
        String nacionalidad;
        String pseudonimo;

        out.println("Digite el nombre");
        nombre = in.readLine();
        if (gestor.autorExiste(nombre)) {
            out.println("El autor ya esta registrado");
        } else {
            out.println("Digite la nacionalidad");
            nacionalidad = in.readLine();
            out.println("Digite el pseudonimo");
            pseudonimo = in.readLine();
            gestor.registrarAutores(nombre, nacionalidad, pseudonimo);
            out.println("Autor registrado con éxito!");
        }

    }

    public static void registrarNovela() throws IOException {
        String isbn;
        String nombre;
        int annioP;
        String nombreAutor;
        double precio;
        if (gestor.listarAutores().length == 0) {
            out.println("No hay autores registrados");
        } else {
            out.println("Digite el isbn");
            isbn = in.readLine();
            if (gestor.libroExiste(isbn)) {
                out.println("El libro ya fue registrado");
            } else {
                out.println("Digite el nombre del libro");
                nombre = in.readLine();
                out.println("Digite el año de publicación");
                annioP = Integer.parseInt(in.readLine());
                out.println("Digite el nombre del autor");
                nombreAutor = in.readLine();
                while (!gestor.autorExiste(nombreAutor)) {
                    out.println("El autor registrado no existe , intentelo denuevo");
                    out.println("Digite el nombre del autor");
                    nombreAutor = in.readLine();
                }
                out.println("Digite el precio");
                precio = Double.parseDouble(in.readLine());
                gestor.registrarNovela(isbn, nombre, annioP, nombreAutor, precio);
                out.println("Novela registrada con éxito!");

            }
        }

    }

    public static void registrarCuento() throws IOException {
        String isbn;
        String nombre;
        int annioP;
        String nombreAutor;
        double precio;
        String rangoEdad;
        if (gestor.listarAutores().length == 0) {
            out.println("No hay autores registrados");
        } else {
            out.println("Digite el isbn");
            isbn = in.readLine();
            if (gestor.libroExiste(isbn)) {
                out.println("El libro ya fue registrado");
            } else {
                out.println("Digite el nombre del libro");
                nombre = in.readLine();
                out.println("Digite el año de publicación");
                annioP = Integer.parseInt(in.readLine());
                out.println("Digite el nombre del autor");
                nombreAutor = in.readLine();
                while (!gestor.autorExiste(nombreAutor)) {
                    out.println("El autor digitado no existe , intentelo denuevo");
                    out.println("Digite el nombre del autor");
                    nombreAutor = in.readLine();
                }
                out.println("Digite el precio");
                precio = Double.parseDouble(in.readLine());
                out.println("Digite el rango de edad");
                rangoEdad = in.readLine();
                gestor.registrarCuento(isbn, nombre, annioP, nombreAutor, precio, rangoEdad);
                out.println("Cuento registrado con éxito!");
            }
        }

    }

    public static void listarAutores() throws IOException {
        if (gestor.listarAutores().length == 0) {
            out.println("No hay autores registrados!");
        } else {
            out.println("Lista de autores: ");
            for (String dato : gestor.listarAutores()) {
                out.println(dato);
            }
        }

    }

    public static void listarLibros() throws IOException {
        if (gestor.listarLibros().length == 0) {
            out.println("No hay libros registrados!");
        } else {
            out.println("Lista de libros: ");
            for (String dato : gestor.listarLibros()) {
                out.println(dato);
            }
        }

    }
}
